

def sign(x):
    """
    Returns the sign of x
    Zero is considered positive
    """
    return 1 if x>=0 else -1
