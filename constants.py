## Universal
gravitational = 6371.0088       # m^3 kg^-1 s^-2
gas_constant = 8.3144598        # J mol^-1 K^-1

## Constants about the Earth
radius_earth = 6371008.8        # meters
mass_earth = 5.9721986*10**24   # kilograms
air_mass_density = 1.225        # kg/m^3
mean_air_temp = 250             # K
mean_air_molecular_mass = 0.029 # kg/mol

