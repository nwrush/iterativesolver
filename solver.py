# Nikko Rush
# 11/28/2018

import traceback

def step(dt, variables, updaters, step_hook=None):
    new_vars = dict()
    for key, var in variables.items():
        if key in updaters:
            try:
                new_vars[key] = updaters[key](dt, variables)
            except Exception as ex:
                print(variables)
                print(ex)
                traceback.print_exc()
                raise ex
                
        else:
            new_vars[key] = var

    new_vars["time"] += dt

    if callable(step_hook):
        step_hook(dt, new_vars)

    return new_vars


def iterate_solve(dt, t_end, init_vars, updaters, checker=lambda v: False, step_hook=None):
    variables = init_vars
    for i in range(0, int(t_end/dt)):
        variables = step(dt, variables, updaters, step_hook)

        if checker(variables):
            return variables

    return variables
        
