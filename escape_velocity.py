# Nikko Rush
# 11/28/2018

import constants
import solver
import solver_math as smath

import csv
import math

"""
variable keys: {time, altitude (above surface), height (above COM earth), velocity, acceleration, fd (force of drag), escV (escape velocity at altitude), air_density
constants, mass, area, drag_coeff
}
"""



def velocity_change(dt, variables):
    v = variables["velocity"]
    return v + variables["acceleration"]*dt


def acceleration_change(dt, variables):
    return (-1)*smath.sign(variables["velocity"])*variables["fd"]/variables["mass"] - 9.8


def force_drag_change(dt, variables):
    return 0.5*constants.air_mass_density*(variables["velocity"]**2)*variables["drag_coeff"]*variables["area"]


def altitude_change(dt, variables):
    return variables["altitude"]+variables["velocity"]*dt


def height_change(dt, variables):
    return variables["height"]+variables["velocity"]*dt


def escV_change(dt, variables):
    return math.sqrt(2*constants.gravitational*constants.mass_earth/variables["height"])


def air_density_change(dt, variables):
    scale_height = constants.gas_constant * constants.mean_air_temp/(constants.mean_air_molecular_mass*9.8)
    return 1.225*math.exp(-1*variables["altitude"]/scale_height)


"""
variable keys: {time, altitude (above surface), height (above COM earth), velocity, acceleration, fd (force of drag), escV (escape velocity at altitude), air_density
constants, mass, area, drag_coeff
}
"""

def constraint_check(variables):
    if variables["velocity"] < 0 or variables["escV"] <= variables["velocity"]:
        print("ending early")
        return True
    else:
        return False
    # return variables["velocity"] < 0 or variables["escV"] <= variables["velocity"]


def step_hook_gen(init_vars, fname):
    f = open(fname, 'w', newline='')
    writer = csv.DictWriter(f, fieldnames=init_vars.keys())

    writer.writeheader()
    def step_hook(dt, vars):
        writer.writerow(vars)

    return f, step_hook


def main():
    area = 0.5
    v_o = 100000000
    a_o = -9.8
    f_o = 0
    alt_o = 0
    h_o = constants.radius_earth

    escV_o = math.sqrt(2*constants.gravitational*constants.mass_earth/h_o)
    p_o = 1.225                  # air density

    init_vars = {
        "time": 0,
        "altitude": a_o,
        "height": h_o,
        "velocity": v_o,
        "acceleration": a_o,
        "fd": f_o,
        "escV": escV_o,
        "air_density": p_o,
        "mass": 1000000,
        "area": area,
        "drag_coeff": 0.3
    }

    updaters = {
        "altitude": altitude_change,
        "height": height_change,
        "velocity": velocity_change,
        "acceleration": acceleration_change,
        "fd": force_drag_change,
        "escV": escV_change,
        "air_density": air_density_change
    }

    csvfile, hook = step_hook_gen(init_vars, "escV.csv")
    sol = solver.iterate_solve(0.001, 2000, init_vars, updaters, constraint_check, step_hook=hook)
    print(sol)
    csvfile.close()


if __name__ == "__main__":
    main()
